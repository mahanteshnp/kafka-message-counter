package com.five9;

import com.five9.event.agent.AgentStateEvent;
import com.five9.event.envelope.Five9Event;
import com.five9.event.interaction.InteractionEvent;
import com.five9.event.interaction.ReconcileAssignedToAgentReset;
import com.five9.event.interaction.ReconcileInQueueReset;
import com.five9.event.interaction.ReconcileInteractionsAssignedToAgent;
import com.five9.event.interaction.ReconcileInteractionsInQueue;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer {

    private static final Logger LOG = LoggerFactory.getLogger(Consumer.class);

    private static final String TOPIC = "topic";

    private static final String GROUP = "group";

    private static final String BOOTSTRAP_SERVER = "bootstrap-server";

    private static final String PRINT_TIMESTAMP = "print-timestamp";

    private static final String TOPIC_OFFSET = "offset";

    private static final String START_TIME = "start-time";

    private static final String END_TIME = "end-time";

    private String topicName;

    private long startTime;

    private long endTime;

    private Long campaignIdFilter;


    private Properties consumerProperties;

    private boolean stopNow = false;

    private int interactionEventcount = 0;

    private int agentStateEventCOunt = 0;

    private int reconcileEventCount = 0;

    private int totalEventCount = 0;

    private long endOffset = 0;

    private long startOffset =0 ;

    private KafkaConsumer<String, byte[]> kafkaConsumer;

    private void start() {
        kafkaConsumer = new KafkaConsumer<>(consumerProperties);
        TopicPartition topicPartition = new TopicPartition(topicName, 0);
        kafkaConsumer.assign(Collections.singletonList(topicPartition));
        boolean stopNow = false;
        startOffset = getOffset(startTime,topicPartition);
        endOffset  = getOffset(endTime , topicPartition);
        kafkaConsumer.seek(topicPartition , startOffset);
        System.out.println("startOffset - " + startOffset);
        System.out.println("endOffset - " + endOffset);

        try {
            while (!stopNow) {

                ConsumerRecords<String, byte[]> records = kafkaConsumer.poll(Integer.MAX_VALUE);
                if (records.isEmpty()) {
                    continue;
                }
                for (ConsumerRecord<String, byte[]> record : records) {
                    if(record.offset() == endOffset){
                        stopNow = true;
                    }
                    parseFive9Event(record.value()).filter(this::filterRecord);

                    if (stopNow) {
                        System.out.println("count of interactionevent messages " + interactionEventcount);
                        System.out.println("count of agentstateevent messages " + agentStateEventCOunt);
                        System.out.println("count of reconcileevent messages " + reconcileEventCount);
                        System.out.println("count of all messages " + totalEventCount);
                        break;
                    }
                }
            }
        } finally {
            kafkaConsumer.close();
        }
    }

    private boolean filterRecord( Five9Event five9Event )
    {
           totalEventCount++;
            if (five9Event.getBody().is(InteractionEvent.class)) {
                interactionEventcount++;
                        return true;
                }
            else if(five9Event.getBody().is(AgentStateEvent.class)){
                agentStateEventCOunt++;
                        return true;
            }
            else if(five9Event.getBody().is(ReconcileAssignedToAgentReset.class)
            || five9Event.getBody().is(ReconcileInteractionsAssignedToAgent.class)
            || five9Event.getBody().is(ReconcileInQueueReset.class)
            || five9Event.getBody().is(ReconcileInteractionsInQueue.class))  {
                reconcileEventCount ++;
                return true;
            }
        return false;
    }


    private static Properties getConsumerProperties(String bootstrapServers, String groupId, String offset) {
        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        props.put("group.id", groupId);

        props.put("enable.auto.commit", "true");
        if (offset != null && !offset.equals("latest")) {
            props.put("auto.offset.reset", offset);
        }
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");

        return props;
    }



    private Optional<Five9Event> parseFive9Event(byte[] record) {
        try {
            return Optional.of(Five9Event.parseFrom(record));
        } catch (InvalidProtocolBufferException e) {
            LOG.error("", e);
            return Optional.empty();
        }
    }

    private static void print(String string) {
        System.out.println(string);
    }


    private static Optional<CommandLine> parseCmd(String[] args) {
        Options options = new Options();

        Option bootstrapServers = new Option("bs", BOOTSTRAP_SERVER, true, "Bootstrap servers for Kafka cluster");
        bootstrapServers.setRequired(true);
        options.addOption(bootstrapServers);

        Option groupId = new Option("g", GROUP, true, "Kafka consumer group id");
        groupId.setRequired(false);
        options.addOption(groupId);

        Option topic = new Option("t", TOPIC, true, "Kafka topic name");
        topic.setRequired(true);
        options.addOption(topic);

        Option start_time = new Option("st", START_TIME, true, "Messages published after this time will be consumed");
        start_time.setRequired(true);
        options.addOption(start_time);

        Option end_time = new Option("et", END_TIME, true, "Messages published after this time will not be consumed");
        start_time.setRequired(true);
        options.addOption(end_time);

        Option offset = new Option(null, TOPIC_OFFSET, true, "Topic offset:  latest (default), earliest");
        offset.setRequired(false);
        options.addOption(offset);

        CommandLineParser parser = new DefaultParser();

        try {
            return Optional.ofNullable(parser.parse(options, args));
        } catch (ParseException e) {
            LOG.error(e.getMessage());

            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar build/libs/kafka-message-counter.jar",options);

            return Optional.empty();
        }
    }

    private static Optional<Consumer> fromCommandLine(CommandLine cmd) {
        Consumer result = new Consumer();
        result.topicName = cmd.getOptionValue(TOPIC);
        result.consumerProperties = getConsumerProperties(cmd.getOptionValue(BOOTSTRAP_SERVER), getGroupId(cmd),
            cmd.getOptionValue(TOPIC_OFFSET));
        result.startTime = Long.parseLong(cmd.getOptionValue(START_TIME));
        result.endTime = Long.parseLong(cmd.getOptionValue(END_TIME));

        return Optional.of(result);
    }

    private static String getGroupId(CommandLine cmd) {
        String groupId = cmd.getOptionValue(GROUP);

        if (groupId == null) {
            groupId = UUID.randomUUID().toString();
            print("Generated random groupId: " + groupId);
        }
        return groupId;
    }

    private  long getOffset(long timeInMs  , TopicPartition partition){
        Map<TopicPartition, Long> mapPartitionTOTime = new HashMap<>();
        mapPartitionTOTime.put(partition, timeInMs);
        Map<TopicPartition, OffsetAndTimestamp> startoffset = kafkaConsumer.offsetsForTimes(mapPartitionTOTime);
        System.out.println(startoffset);
        return startoffset.get(partition).offset();
    }

    public static void main(String[] args) {

        Optional<Consumer> consumer = Optional.ofNullable(args)
            .flatMap(Consumer::parseCmd)
            .flatMap(Consumer::fromCommandLine);
        consumer.ifPresent(c -> c.start());
    }
}
