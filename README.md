Usage:

run below commands from root directory.

- ./gradlew clean build
-  java -jar build/libs/kafka-message-counter.jar 
    -bs,--bootstrap-server<arg>     Bootstrap servers for Kafka cluster
    -et,--end-time <arg>            Messages published after this time will
                                    not be consumed
    -st,--start-time <arg>          Messages published after this time will be
                                    consumed
    -t,--topic <arg>                Kafka topic name
    
Example :

java -jar build/libs/kafka-message-counter.jar 
--bootstrap-server qafdskafka011.scl.five9lab.com:9092,qafdskafka012.scl.five9lab.com:9092,qafdskafka013.scl.five9lab.com:9092 
--topic org_1_raw 
--start-time 1591984800000 
--end-time 1591987800000